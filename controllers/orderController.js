const User = require('../models/user');
const Product = require('../models/product');
const Order = require('../models/order');
const auth = require('../auth')


let totalAmt = 0;


// ------Place an order
module.exports.addOrder =  (reqBody, userData) => {

    return User.findById(userData.userId).then((result) => {
        if(userData.isAdmin){
            return  'Admin is not allowed to make an order'
        } else {
            let newOrder = new Order({
                userId: userData.userId, 
                productId: reqBody.productId,
                quantity: reqBody.quantity,
                price: reqBody.price,
                totalAmount: reqBody.quantity * reqBody.price
            });

            return  newOrder.save().then((order, error)=> {
                if(error) {
                    return false
                }

                else {
                    return true
                }
            })
        }
    })
};




// Retrieve all customers - admin only
module.exports.retrieveAllOrders = (data) => {
    if(data.isAdmin){
        return Order.find({}).then(result =>{
            return result
        })
    } else {
        return false
    }
};

