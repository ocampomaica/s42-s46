const User = require('../models/user');
const Product = require('../models/product');
const bcrypt = require('bcrypt');
const auth = require('../auth')

// user registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return User.find({email:reqBody.email}).then(users => {
		if(users.length == 0) {
			return newUser.save().then((user, error) => {
				if (error) {
					return false
				} else {
					return "Account created successfully"
				}
			})
		} else {
            return "Email is already exists."
        }
    })
};



//login user
module.exports.loginUser = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
    	if (result == null) {
    		return false
    	} else {
    		const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

    		if (isPasswordCorrect) {
    			return { access: auth.createAccessToken(result) }
    		} else {
    			return "Incorrect password"
            }
        }
    })
}


// retrieve user's own data
// retrieve user's own data
module.exports.retrieveUserDetails = (userData) => {
    return User.findById(userData.id).then(userDetails => {

        let showUserDetails;

        if (userData.isAdmin) {
        	showUserDetails = {
                email: userDetails.email
            }
        } else {
        	showUserDetails = {
                email: userDetails.email,
                shippingInfo: userDetails.shippingInfo
            }
        }
        return showUserDetails
    })
};

//retrieve all user's data
module.exports.getAllUser = (isAdminData) => {
    return User.find({ isAdmin: false }, { _id: 0, firstName: 1, lastName: 1, email: 1 }).then(users => {

        if (isAdminData) {

            return users
        } else {
            return "You are not allowed to view other customers' details."

        }
    })
};


// update user's own information
module.exports.updateUserInfo = (reqParams, reqBody, isAdminData) => {
	return User.findById(reqParams.userId).then(result => {
		if(isAdminData) {
			return "This action is disabled for admins."
		} else {
			let shippingInfo = {
				recipientsName: reqBody.recipientsName,
				mobileNum: reqBody.mobileNum,
				addressLine: reqBody.addressLine,
				city: reqBody.city,
				region: reqBody.region,
				country: reqBody.country
			}

			return User.findById(reqParams.userId).then((user) => {
				user.shippingInfo = [];
				user.shippingInfo.push(shippingInfo);
				return user.save().then((user, error) => {

					if(error) {
						return false
					} else {
						return "Information Successfully Updated!"
					}
				})
			})
		}
	})
}

/*module.exports.updateUserInfo = (reqBody, userData) => {
 
    return User.findById(userData.id).then(result => {
        if (userData.isAdmin) {

            return User.findById(userData.id).then((user) => {

                if(reqBody.recipientsName) {
                user.recipientsName = reqBody.recipientsName;
                }

                return user.save().then((user, error) => {

                    if (error) {
                        return false
                    } else {
                        return true
                    }
                })


            }) 

        } else { 

            let customerInfo = {
                firstName: reqBody.firstName,
                lastName: reqBody.lastName
            }
       

            return User.findById(userData.id).then((user) => {
                if(reqBody.firstName) {
                user.firstName = customerInfo.firstName;
                }

                if(reqBody.lastName) {
                user.lastName = customerInfo.lastName;
                }

                if(reqBody.shippingInfo) {
                user.shippingInfo = reqBody.shippingInfo;   
                }                 



                return user.save().then((user, error) => {

                    if (error) {
                        return false
                    } else {
                        return true
                    }
                })


            })

        }

    })
}*/



// set user as admin
module.exports.setAdmin = (reqParams, reqBody, isAdminData) => {

    let newAdmin = {
        isAdmin: reqBody.isAdmin
    }
    return User.findByIdAndUpdate(reqParams.userId, newAdmin).then(user => {

        if (isAdminData) {
            return "Role updated"

        } else {
            return "This action is disabled for non-admins."

        }
    })
};






// retrieve user's order (not working)
module.exports.orderHistory = (userData) => {
    return Order.find({ userId: userData.id }, { _id: 0, userId: 0, __v: 0 }).then(orders => {

    	if (userData.isAdmin == false) {
            if (orders.length > 0) {
            	return orders
            } else {
                return "You do not have any orders yet."
            }
        } else {
            return "Admins do not have orders."
        }
    })
};

