const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth')


// create product (admin only)
router.post('/', auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	productController.addProduct(req.body, {
		userId: userData.id,
		isAdmin: userData.isAdmin
	}).then(resultFromController => res.send(resultFromController))
})

// retrieve all active product
router.get('/', (req,res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
})

// retrieve single product
router.get('/:productId', (req,res) => {
	console.log(req.params.productId)

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})


// update product information (admin only)
router.put('/:productId', auth.verify, (req,res) => {
	const isAdminData = auth.decode(req.headers.authorization)

	productController.updateProduct(req.params, req.body, isAdminData).then(resultFromController => res.send(resultFromController))
})


// archive product (admin only)
router.put('/archive/:productId', auth.verify, (req,res)=>{
	const isAdminData = {
		productId: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.archiveProduct(isAdminData, req.body).then(resultFromController => res.send(resultFromController))
});



module.exports = router