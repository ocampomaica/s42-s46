const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Enter your email"]
	},
	password: {
		type: String,
		required: [true, "Enter password!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	shippingInfo: [{
		recipientsName: {
			type: String,
			required: [true, "Enter your name"]
		},
		mobileNum: {
			type: String,
			required: [true, "Enter your mobile number"]
		},
		addressLine: {
			type: String,
			required: [true, "Enter your Unit/Floor number and Street Name"]
		},
		city: {
			type: String,
			required: [true, "Enter your city"]
		},
		region: {
			type: String,
			required: [true, "Enter your region"]
		},
		country: {
			type: String,
			required: [true, "Enter your country"]
		}
	}],
	cart: [{
		productName: {
			type: String,
			required: [true, "Enter the Product Name"]
		},
		price: {
			type: Number,
			required: [true, "Enter the product price"]
		},
		quantity: {
			type: Number,
			required: [true, "Quantity cannot be empty"]
		},
		subtotal: {
			type: Number,
			required: [true, "Subtotal is required"]
		}
	}]
})


module.exports = mongoose.model("User", userSchema)