const mongoose = require('mongoose')


const orderSchema = new mongoose.Schema({
	/*orderNumber: {
		type: String,
		required: [true, 'Order number is required']
	},*/
	userId: {
		type: String,
		required: [true, 'Enter your UserID']
	},
	products: [{
		productId: {
			type: String,
			required: [true, "Enter a product name"]
		},
		quantity: {
			type: Number,
			default: 1
		},
		price: {
			type: Number,
			required: [true, 'Enter the price here']
		}
	}],
	totalAmount: {
		type: Number,
		default: 0
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})


module.exports = mongoose.model('Order', orderSchema)