const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth')


// user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// log in user
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// retrieve user's own data
router.get("/myData", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.retrieveUserDetails(userData).then(resultFromController => res.send(resultFromController))
})


//retrieve all user's data
router.get("/customers", auth.verify, (req, res) => {

    const isAdminData = auth.decode(req.headers.authorization).isAdmin

    userController.getAllUser(isAdminData).then(resultFromController => res.send(resultFromController))
})



// update user's own information
router.put("/updateInfo/:userId", auth.verify, (req, res) => {
	const isAdminData = auth.decode(req.headers.authorization).isAdmin

	userController.updateUserInfo(req.params, req.body, isAdminData).then(resultFromController => res.send(resultFromController))
})


// set user as admin
router.put("/addAdmin/:userId", auth.verify, (req, res) => {

    const isAdminData = auth.decode(req.headers.authorization).isAdmin

    userController.setAdmin(req.params, req.body, isAdminData).then(resultFromController => res.send(resultFromController))
})




// retrieve user's order
router.get("/orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

    userController.orderHistory(userData).then(resultFromController => res.send(resultFromController))
})






module.exports = router