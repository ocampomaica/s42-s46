const User = require('../models/user');
const Product = require('../models/product');
const auth = require('../auth')

// create product (admin only)
module.exports.addProduct = (reqBody, userData) => {

	return User.findById(userData.userId).then(result => {
		if(userData.isAdmin == false) {
			return "Only an admin can add a product"
		} else {
			let newProduct = new Product({
				productName: reqBody.productName,
				category: reqBody.category,
				description: reqBody.description,
				price: reqBody.price
			});

			return newProduct.save().then((product, error) => {
				if(error) {
					return false
				} else {
					return "Product added successfully"
				}
			})
		}
	})
};


// retrieve all active product
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}


// retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}


// update product information (admin only)
module.exports.updateProduct = async(reqParams, reqBody, userData) => {

	if(userData.isAdmin === true) {
		let updatedProduct = {
			productName: reqBody.productName,
			description: reqBody.description,
			price: reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((updatedProduct, error) => {
			if(error) {
				return false
			} else {
				return "Product successfully updated"
			}
		})
	} else {
		return await "Only admins are allowed to update a product/s"
	}

}

// archive product
module.exports.archiveProduct = async (userData, reqBody) =>{
	if(userData.isAdmin === true) {
		let archivedProduct = {
			isActive: reqBody.isActive
		}

		return await Product.findByIdAndUpdate(userData.productId, archivedProduct).then((archivedProduct, error)=>{
			if(error){
				return false
			} else {
				return true
			}
		})
	} else {
		return await 'Only admins are allowed to archive a product'
	}
}

