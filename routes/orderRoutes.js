const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth')


router.post('/', auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)

    orderController.addOrder(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send(resultFromController))
});


// Retrieve all orders - admin only
router.get('/all', auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)

	orderController.retrieveAllOrders(userData).then(resultFromController => res.send(resultFromController))
});




module.exports = router