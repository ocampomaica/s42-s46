const mongoose = require('mongoose')


const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Enter a product name"]
	},
	category: {
		type: String,
		required: [true, "Enter a product category"]
	},
	description: {
		type: String,
		required: [true, "Enter a product description"]
	},
	price: {
		type: Number,
		required: [true, "Enter a product price"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}

})


module.exports = mongoose.model('Product', productSchema)